INSERT INTO dicts_specie (id, sys_order, codesp, sp_true, sci_name, full_name, common_name_fr, common_name_eng)
VALUES
  (1, 1, 'rhifer', TRUE, 'Rhinolophus ferrumequinum', 'Rhinolophus ferrumequinum (Schreber, 1774)', 'Grand rhinolophe',
   'Greater Horseshoe Bat'),
  (2, 2, 'rhihip', TRUE, 'Rhinolophus hipposideros', 'Rhinolophus hipposideros (Bechstein, 1800)', 'Petit rhinolophe', 'Lesser Horseshoe Bat'),
  (3, 3, 'rhieur', TRUE, 'Rhinolophus euryale', 'Rhinolophus euryale Blasius, 1853', 'Rhinolophe euryale', 'Mediterranean Horseshoe Bat'),
  (4, 4, 'rhimeh', TRUE, 'Rhinolophus mehelyi', 'Rhinolophus mehelyi Matschie, 1901', 'Rhinolophe de Mehely', 'Mehely''s Horseshoe Bat'),
  (5, 100, 'rhieurfer', FALSE, 'Rhinolophus euryale / ferrumequinum', NULL, 'Grand rhinolophe / Rhinolophe euryale', NULL),
  (6, 0, 'rhieurhip', FALSE, 'Rhinolophus euryale / hipposideros', NULL, 'Petit rhinolophe / euryale', NULL),
  (7, 101, 'rhiind', FALSE, 'Rhinolophus sp.', NULL, 'Rhinolophe indéterminé', NULL),
  (8, 5, 'myomys', TRUE, 'Myotis mystacinus', 'Myotis mystacinus (Kuhl, 1817)', 'Murin à moustaches', 'Whiskered Bat'),
  (9, 6, 'myobra', TRUE, 'Myotis brandtii', 'Myotis brandtii (Eversmann, 1845)', 'Murin de Brandt', 'Brandt''s Bat'),
  (10, 7, 'myoalc', TRUE, 'Myotis alcathoe', 'Myotis alcathoe Helversen & Heller, 2001', 'Murin d''Alcathoé', 'Alcathoe Myotis , Alcathoe whiskered Bat'),
  (11, 8, 'myoema', TRUE, 'Myotis emarginatus', 'Myotis emarginatus (E. Geoffroy, 1806)', 'Murin à oreilles échancrées', 'Notch-eared Bat'),
  (12, 9, 'myonat', TRUE, 'Myotis nattereri', 'Myotis nattereri (Kuhl, 1817)', 'Murin de Natterer', 'Natterer''s Bat'),
  (13, 0, 'myoesc', TRUE, 'Myotis escalerai', 'Myotis escalerai Cabrera, 1904', 'Murin d''Escalera', NULL),
  (14, 10, 'myobec', TRUE, 'Myotis bechsteinii', 'Myotis bechsteinii (Kuhl, 1817)', 'Murin de Bechstein', 'Bechstein''s Bat'),
  (15, 11, 'myomyo', TRUE, 'Myotis myotis', 'Myotis myotis (Borkhausen, 1797)', 'Grand Murin', 'Mouse-eared Bat'),
  (16, 12, 'myobly', TRUE, 'Myotis blythii', 'Myotis blythii (Tomes, 1857)', 'Petit Murin', 'Lesser mouse-eared Bat , Lesser Mouse-eared Myotis'),
  (17, 13, 'myopun', TRUE, 'Myotis punicus', 'Myotis punicus Felten, Spitzenberger & Storch, 1977', 'Murin du Magrheb', 'Maghrebian Myotis , Maghreb mouse-eared Bat'),
  (18, 103, 'murmur', FALSE, 'Myotis myotis / M. blythii', NULL, '"Murin de ""grande taille"""', NULL),
  (19, 14, 'myodau', TRUE, 'Myotis daubentonii', 'Myotis daubentonii (Kuhl, 1817)', 'Murin de Daubenton', 'Daubenton''s Bat'),
  (20, 15, 'myocap', TRUE, 'Myotis capaccinii', 'Myotis capaccinii (Bonaparte, 1837)', 'Murin de Capaccini', 'Long-fingered Bat'),
  (21, 16, 'myodas', TRUE, 'Myotis dasycneme', 'Myotis dasycneme (Boie, 1825)', 'Murin des marais', 'Pond Bat'),
  (22, 119, 'myoalcbramys', FALSE, 'Myotis alcathoe / brandtii / mystacinus', NULL, 'Murin d''Alcathoé / Brandt / moustaches', NULL),
  (23, 0, 'myoalccapmys', FALSE, 'Myotis alcathoe / capaccini / mystacinus', NULL, 'Murin d''Alcathoé / Capaccini / moustaches', NULL),
  (24, 0, 'myoalcdau', FALSE, 'Myotis alcathoe / daubentonii', NULL, 'Murin d''Alcathoe / de Daubenton', NULL),
  (25, 123, 'myoalcema', FALSE, 'Myotis alcathoe / emarginatus', NULL, 'Murin d''Alcathoé / émarginé', NULL),
  (26, 122, 'myoalcemamys', FALSE, 'Myotis alcathoe / emarginatus / mystacinus', NULL, 'Murin d''Alcathoé / émarginé / moustaches', NULL),
  (27, 118, 'myoalcmys', FALSE, 'Myotis alcathoe / mystacinus', NULL, 'Murin d''Alcathoé / moustaches', NULL),
  (28, 116, 'myobecbra', FALSE, 'Myotis bechsteinii / brandtii', NULL, 'Murin de Bechstein / Brandt', NULL),
  (29, 115, 'myobecbradau', FALSE, 'Myotis bechsteinii / brandtii / daubentonii', NULL, 'Murin de Bechstein / Brandt / Daubenton', NULL),
  (30, 0, 'myobecbraema', FALSE, 'Myotis bechsteinii / brandtii / emarginatus', NULL, 'Murin de Bechstein / Brandt / émarginé', NULL),
  (31, 0, 'myobecbramur', FALSE, 'Myotis bechsteinii / brandtii / myotis-bly', NULL, '"Murin de Bechstein / Brandt / ""grande taille"""', NULL),
  (32, 0, 'myobecbramys', FALSE, 'Myotis bechsteinii / brandtii / mystacinus', NULL, 'Murin de Bechstein / Brandt / moustaches', NULL),
  (33, 114, 'myobecdau', FALSE, 'Myotis bechsteinii / daubentonii', NULL, 'Murin de Bechstein / Daubenton', NULL),
  (34, 0, 'myobecdaumur', FALSE, 'Myotis bechsteinii / daubentonii / myotis-bly', NULL, '"Murin de Bechstein /Daubenton/""grande taille"""', NULL),
  (35, 112, 'myobecdaumys', FALSE, 'Myotis bechsteinii / daubentonii / mystacinus', NULL, 'Murin de Bechstein / Daubenton / moustaches', NULL),
  (36, 117, 'myobecema', FALSE, 'Myotis bechsteinii / emarginatus', NULL, 'Murin de Bechstein / émarginé', NULL),
  (37, 0, 'myobecmur', FALSE, 'Myotis bechsteinii / myotis-blythii', NULL, '"Murin de Bechstein / murin de ""grande taille"""', NULL),
  (38, 0, 'myobradau', FALSE, 'Myotis brandtii / daubentonii', NULL, 'Murin de Brandt / Daubenton', NULL),
  (39, 0, 'myobradaumys', FALSE, 'Myotis brandtii / daubentonii / mystacinus', NULL, 'Murin de Brandt / Daubenton / moustaches', NULL),
  (40, 0, 'myobraema', FALSE, 'Myotis brandtii / emarginatus', NULL, 'Murin de Brandt / échancré', NULL),
  (41, 0, 'myobraemamys', FALSE, 'Myotis brandtii / emarginatus / mystacinus', NULL, 'Murin de Brandt / émarginé / moustaches', NULL),
  (42, 0, 'myobramys', FALSE, 'Myotis brandtii / mystacinus', NULL, 'Murin de Brandt / moustaches', NULL),
  (43, 0, 'myocapdau', FALSE, 'Myotis capaccinii / daubentonii', NULL, 'Murin de Capaccini / de Daubenton', NULL),
  (44, 0, 'myocapdauema', FALSE, 'Myotis capaccini / daubentonii / emarginatus', NULL, 'Murin de Capaccini / Daubenton / émarginé', NULL),
  (45, 0, 'myodauema', FALSE, 'Myotis daubentonii / emarginatus', NULL, 'Murin de Daubenton / émarginé', NULL),
  (46, 113, 'myodauemamys', FALSE, 'Myotis daubentonii / mystacinus / emarginatus', NULL, 'Murin de Daubenton / moustaches / emarginé', NULL),
  (47, 120, 'myodaumys', FALSE, 'Myotis daubentonii / mystacinus', NULL, 'Murin de Daubenton / moustaches', NULL),
  (48, 121, 'myoemamys', FALSE, 'Myotis emarginatus / mystacinus', NULL, 'Murin émarginé / moustaches', NULL),
  (49, 0, 'myonatbly', FALSE, 'Myotis natterreri / blythii', NULL, 'Murin de Natterer / Petit murin', NULL),
  (50, 102, 'petmur', FALSE, 'Myotis sp.', 'Myotis sp.', '"Murin de ""petite taille"""', NULL),
  (51, 0, 'myotis', FALSE, 'Myotis sp.', 'Myotis sp.', 'Murin indét. (PETMUR/MURMUR)', NULL),
  (52, 17, 'nyclas', TRUE, 'Nyctalus lasiopterus', 'Nyctalus lasiopterus (Schreber, 1780)', 'Grande Noctule', 'Giant Noctule , Greater Noctule Bat'),
  (53, 18, 'nyclei', TRUE, 'Nyctalus leisleri', 'Nyctalus leisleri (Kuhl, 1817)', 'Noctule de Leisler', 'Leisler''s Bat'),
  (54, 19, 'nycnoc', TRUE, 'Nyctalus noctula', 'Nyctalus noctula (Schreber, 1774)', 'Noctule commune', 'Noctule Bat'),
  (55, 124, 'nycnoclas', FALSE, 'Nyctalus noctula / N. lasiopterus', NULL, 'Noctule commune / Grande noctule', NULL),
  (56, 104, 'nycind', FALSE, 'Nyctalus sp.', NULL, 'Noctule indéterminée', NULL),
  (57, 0, 'nycleivesmur', FALSE, 'Nyctalus leisleri / Vespertilio murinus', NULL, 'Noctule de Leisler / Sérotine bicolore', NULL),
  (58, 105, 'nyctad', FALSE, 'Nyctalus / Tadarida', NULL, 'Noctule / Molosse', NULL),
  (59, 20, 'eptser', TRUE, 'Eptesicus serotinus', 'Eptesicus serotinus (Schreber, 1774)', 'Sérotine commune', 'Serotine Bat'),
  (60, 21, 'eptnil', TRUE, 'Eptesicus nilssonii', 'Eptesicus nilssonii (Keyserling & Blasius, 1839)', 'Sérotine de Nilsson', 'Northern Bat'),
  (61, 22, 'vesmur', TRUE, 'Vespertilio murinus', 'Vespertilio murinus Linnaeus, 1758', 'Sérotine bicolore', 'Parti-coloured Bat'),
  (62, 106, 'eptind', FALSE, 'Eptesicus sp.', NULL, 'Sérotine indéterminée', NULL),
  (63, 107, 'eptnyc', FALSE, 'Eptesicus / Nyctalus', NULL, 'Sérotine / Noctule', NULL),
  (64, 108, 'eptvesnyc', FALSE, 'Eptesicus / Vespertilio / Nyctalus', NULL, 'Sérotine / Noctule', NULL),
  (65, 109, 'eptmur', FALSE, 'Eptesicus / Myotis myotis / M. blythii', NULL, '"Sérotine / Murin de ""grande taille"""', NULL),
  (66, 23, 'pippip', TRUE, 'Pipistrellus pipistrellus', 'Pipistrellus pipistrellus (Schreber, 1774)', 'Pipistrelle commune', 'Common Pipistrelle'),
  (67, 24, 'pippyg', TRUE, 'Pipistrellus pygmaeus', 'Pipistrellus pygmaeus (Leach, 1825)', 'Pipistrelle pygmée', 'Pygmy Pipistrelle , Soprano Pipistrelle'),
  (68, 25, 'pipnat', TRUE, 'Pipistrellus nathusii', 'Pipistrellus nathusii (Keyserling & Blasius, 1839)', 'Pipistrelle de Nathusius', 'Nathusius''s Bat'),
  (69, 26, 'pipkuh', TRUE, 'Pipistrellus kuhlii', 'Pipistrellus kuhlii (Kuhl, 1817)', 'Pipistrelle de Kuhl', 'Kuhl''s Pipistrelle'),
  (70, 110, 'pipind', FALSE, 'Pipistrellus sp.', NULL, 'Pipistrelle indéterminée', NULL),
  (71, 125, 'pipkuhnat', FALSE, 'P. kuhlii / nathusii', NULL, 'Pipistrelle de Kuhl / Nathusius', NULL),
  (72, 125, 'pippipnat', FALSE, 'P. pipistrellus / nathusii', NULL, 'Pipistrelle commune / Nathusius', NULL),
  (73, 0, 'pippippyg', FALSE, 'P. pipistrellus / pygmaeus', NULL, 'Pipistrelle commune / pygmée', NULL),
  (74, 126, 'pippipminsch', FALSE, 'P. pipistrellus / M. schreibersii', NULL, 'Pipistrelle commune / Minioptère', NULL),
  (75, 127, 'pippygminsch', FALSE, 'P. pygmaeus / M. schreibersii', NULL, 'Pipistrelle pygmée / Minioptère', NULL),
  (76, 27, 'hypsav', TRUE, 'Hypsugo savii', 'Hypsugo savii (Bonaparte, 1837)', 'Vespère de Savi', 'Savi''s Pipistrelle'),
  (77, 28, 'pleaur', TRUE, 'Plecotus auritus', 'Plecotus auritus (Linnaeus, 1758)', 'Oreillard roux', 'Brown Long-eared Bat'),
  (78, 29, 'pleaus', TRUE, 'Plecotus austriacus', 'Plecotus austriacus (J.B. Fischer, 1829)', 'Oreillard gris', 'Grey Long-eared Bat'),
  (79, 30, 'plemac', TRUE, 'Plecotus macrobullaris', 'Plecotus macrobullaris Kuzjakin, 1965', 'Oreillard montagnard', 'Mountain long-eared Bat'),
  (80, 0, 'pleaurmac', FALSE, 'Plecotus auritus / macrobullaris', NULL, 'Oreillard roux / montagnard', NULL),
  (81, 0, 'pleausmac', FALSE, 'Plecotus austriacus / macrobullaris', NULL, 'Oreillard gris / montagnard', NULL),
  (82, 111, 'pleind', FALSE, 'Plecotus sp.', NULL, 'Oreillard indéterminé', NULL),
  (83, 31, 'barbar', TRUE, 'Barbastella barbastellus', 'Barbastella barbastellus (Schreber, 1774)', 'Barbastelle d''Europe', 'Western Barbastelle'),
  (84, 32, 'minsch', TRUE, 'Miniopterus schreibersii', 'Miniopterus schreibersii (Kuhl, 1817)', 'Minioptère de Schreibers', 'Schreiber''s bent-winged Bat , Schreiber''s long-fingered Bat'),
  (85, 33, 'tadten', TRUE, 'Tadarida teniotis', 'Tadarida teniotis (Rafinesque, 1814)', 'Molosse de Cestoni', 'European Free-tailed Bat'),
  (86, 128, 'chiind', FALSE, 'Chiroptera sp.', NULL, 'Chiroptère indéterminé', NULL),
  (87, 129, '0cfil', FALSE, 'Aucune capture filet', NULL, 'Aucune capture filet', NULL),
  (88, 130, '0obs', FALSE, 'Aucune chauve-souris ou trace', NULL, 'Aucune chauve-souris ou trace', NULL),
  (89, 0, '0du', FALSE, 'Aucun contact acoustique', NULL, 'Aucun contact acoustique', NULL),
  (90, 0, 'palaeind', FALSE, 'Palaeophyllophora sp.', NULL, 'Palaeophyllophora sp.', NULL),
  (91, 0, 'rhigri', FALSE, 'Rhinolophus grivensis', NULL, 'Rhinolophus grivensis', NULL),
  (92, 0, 'rhilis', FALSE, 'Rhinolophus lissiensis', NULL, 'Rhinolophus lissiensis', NULL),
  (93, 0, 'rhidel', FALSE, 'Rhinolophus delphinensis', NULL, 'Rhinolophus delphinensis', NULL),
  (94, 0, 'myoboy', FALSE, 'Myotis boyeri', NULL, 'Myotis boyeri', NULL),
  (95, 0, 'myosan', FALSE, 'Myotis sanctialbani', NULL, 'Myotis sanctialbani', NULL),
  (96, 0, 'vesgri', FALSE, 'Vespertilio grivensis', NULL, 'Vespertilio grivensis', NULL),
  (97, 0, 'vesant', FALSE, 'Vespertilio antiqus', NULL, 'Vespertilio antiqus', NULL),
  (98, 0, 'vesnoc', FALSE, 'Vespertillio noctuloides', NULL, 'Vespertillio noctuloides', NULL),
  (99, 0, 'minfos', FALSE, 'Miniopterus fossilis', NULL, 'Miniopterus fossilis', NULL),
  (100, 0, 'tadind', FALSE, 'Tadarida sp.', NULL, 'Tadarida sp.', NULL),
  (101, 0, 'rousse', FALSE, 'Roussette sp.', NULL, 'Roussette sp.', NULL);


INSERT INTO dicts_typeplace (id, code, category, descr) VALUES
  (1, 'open', 'outdoor', 'Site terrestre de plein air'),
  (2, 'aqua', 'outdoor', 'Site aquatique de plein air'),
  (3, 'aaut', 'tree', 'arbre-autre'),
  (4, 'acar', 'tree', 'arbre-carries'),
  (5, 'aeco', 'tree', 'arbre-écorce décollée'),
  (6, 'afis', 'tree', 'arbre-fissure, cassure du bois'),
  (7, 'apic', 'tree', 'arbre-trou de pic'),
  (8, 'atcr', 'tree', 'arbre-tronc creux (vieillissement)'),
  (9, 'baut', 'building', 'bâti-autre ou non connu'),
  (10, 'bbar', 'building', 'bâti-bardages, lambris en façade'),
  (11, 'bcav', 'building', 'bâti-cave'),
  (12, 'bche', 'building', 'bâti-cheminée (conduit)'),
  (13, 'bcom', 'building', 'bâti-combles, grenier'),
  (14, 'bgra', 'building', 'bâti-grange, remise'),
  (15, 'bmur', 'building', 'bâti-mur'),
  (16, 'btoi', 'building', 'bâti-toiture'),
  (17, 'bvol', 'building', 'bâti-volets'),
  (18, 'egli', 'building', 'eglise, chapelle'),
  (19, 'fala', 'cliff', 'falaises (fissures, écailles)'),
  (20, 'gics', 'nest', 'gîte artificiel chauve-souris'),
  (21, 'gioi', 'nest', 'gîte artificiel à oiseaux'),
  (22, 'pont', 'bridge', 'pont'),
  (23, 'saut', 'cave', 'souterrain artificiel autre'),
  (24, 'sgro', 'cave', 'grotte, gouffre (cavité naturelle)'),
  (25, 'smil', 'cave', 'ouvrage militaire (forts, souterrains)'),
  (26, 'smin', 'cave', 'mine, carrière, champignonnière'),
  (27, 'stun', 'cave', 'tunnel, galerie (ouvrages civils)'),
  (28, 'tboi', 'other', 'tas de bois entreposé'),
  (29, 'zzzz', 'other', 'autre type de gîte');

INSERT INTO dicts_gitebataccess (id, descr) VALUES
  (1,'Oui'),
  (2,'Non'),
  (3,'Obstrué');

INSERT INTO dicts_buildcover (id, descr) VALUES
  (1,'Tuile romane'),
  (2,'Tuile autre'),
	(3,'Ardoise'),
	(4,'Tôle'),
	(5,'Bois'),
	(6,'Lauze'),
	(7,'Autre');

INSERT INTO dicts_typedevice (id, contact, code, descr) VALUES
  (1, 'vm', 'mnnorm', 'filet japonais normal'),
  (2, 'vm', 'mnmono', 'Filet japonais monofilament'),
  (3, 'vm', 'mnhair', 'Filet japonais hair'),
  (4, 'vm', 'harptr', 'Harp-trap'),
  (5, 'ph', 'piegphot', 'piège photo'),
  (6, 'du', 'sm2bat', 'SM2BAT'),
  (7, 'du', 'sm2bat+', 'SM2BAT+ (version 384kHz)'),
  (8, 'du', 'sm3bat', 'SM3BAT'),
  (9, 'du', 'sm4bat', 'SM4BAT'),
  (10, 'du', 'em3', 'EM3'),
  (11, 'du', 'em3+', 'EM3+'),
  (12, 'du', 'dodo192', 'Dodotronic usb 192kHz'),
  (13, 'du', 'dodo200', 'Dodotronic usb 200kHz'),
  (14, 'du', 'dodo250', 'Dodotronic usb 250kHz'),
  (15, 'du', 'dodo384', 'Dodotronic usb 384kHz'),
  (16, 'du', 'batcorder', 'Batcorder'),
  (17, 'du', 'batlogger', 'Batlogger'),
  (18, 'du', 'pibat1', 'PiBatRecorder (v1)'),
  (19, 'du', 'pibat2', 'PiBatRecorder (v2)'),
  (20, 'du', 'rhibat', 'RhinoBatLogger'),
  (21, 'du', 'd240x', 'D240x'),
  (22, 'du', 'd200', 'D200'),
  (23, 'du', 'd980', 'D980'),
  (24, 'du', 'd1000x', 'D1000x');


INSERT INTO dicts_propertydomain (id, code, "domain", descr) VALUES
  (1, 'doma', 'public', 'Site ou propriété domanial'),
  (2, 'coll', 'public', 'Site ou propriété de la collectivité'),
  (3, 'priv', 'prive', 'Site privé'),
  (4, 'nd', 'nd', 'Non déterminé');


INSERT INTO dicts_placeprecision (id, code, descr) VALUES
  (1, 'precis', 'localisation précise'),
  (2, 'locality', 'localisation au lieu-dit'),
  (3, 'centroid', 'point central d''une zone ou d''un transect'),
  (4, 'municip', 'localisation à la commune'),
  (5, 'imprec', 'imprécis, au delà de l''échelle communale');

INSERT INTO dicts_contact (id, code, descr) VALUES
  (1, 'vv', 'Vu en visuel'),
  (2, 'vm', 'Vu en main'),
  (3, 'du', 'Contact acoustique'),
  (4, 'te', 'Télémétrie'),
  (10, 'nc', 'Inconnu');

INSERT INTO dicts_method (id, code, contact, descr) VALUES
  (1, 'duan', 'du', 'Détection automatique en zero-crossing'),
  (2, 'duea', 'du', 'Détection automatique en temps réel'),
  (3, 'dudv', 'du', 'Détection en division de fréquence'),
  (4, 'duex', 'du', 'Détection expansion'),
  (5, 'duht', 'du', 'Détection hétérodyne'),
  (6, 'teaz', 'te', 'Télémétrie croisement d''azimut'),
  (7, 'tegi', 'te', 'Télémétrie recherche de gîte'),
  (8, 'tehi', 'te', 'Télémétrie Homing-In, recherche émetteur'),
  (9, 'vmfi', 'vm', 'Capture au filet'),
  (10, 'vmht', 'vm', 'Capture Harp-Trap'),
  (11, 'vmma', 'vm', 'Capture à la main'),
  (12, 'vvam', 'vv', 'Amplificateur de lumière, vision nocturne'),
  (13, 'vves', 'vv', 'Comptage estimé (effectif moyen)'),
  (14, 'vvma', 'vv', 'Lecture d''un marquage (bague, num., couleur)'),
  (15, 'vvmi', 'vv', 'Comptage minimal, partiel, non exhaustif'),
  (16, 'vvnc', 'vv', 'Non comptés'),
  (17, 'vvph', 'vv', 'Comptage sur photo'),
  (18, 'vvpp', 'vv', 'Piège photo'),
  (19, 'vvsg', 'vv', 'Comptage en sortie de gîte'),
  (20, 'vvsu', 'vv', 'Estimation selon surface'),
  (21, 'vvvu', 'vv', 'Comptage précis à vue'),
  (22, 'vvtr', 'vv', 'Traces (charnier, urine, etc.)'),
  (23, 'vvcr', 'vv', 'Cris audibles'),
  (24, 'vvgu', 'vv', 'Guano'),
  (25, 'vmca', 'vm', 'Cadavre récent'),
  (26, 'vmro', 'ro', 'Restes osseux');

INSERT INTO dicts_countprecision (id, code, contact ,descr) VALUES
  (1, 'precis', 'vv','comptage précis'),
  (2, 'estime', 'vv','comptage estimé'),
  (3, 'mini', 'vv','minimum'),
  (4, 'acoust', 'du','contacts acoustiques'),
  (5, 'telem', 'te','donnée télémétrique');


INSERT INTO dicts_countunit (id, code, contact, descr) VALUES
  (1, 'ind', 'vv','Nombre d''individu(s)'),
  (2, 'azi', 'te','azimut télémétrique'),
  (3, 'minp', 'du','Minute positive'),
  (4, 'hourp', 'du','Heure positive'),
  (5, 'jourp', 'du','Nuit positive'),
  (6, 'c5min', 'du','Contacts par minute (5s)'),
  (7, 'c15min', 'du','Contacts par minute (15s)'),
  (8, 'c5hour', 'du','Contacts par minute (5s)'),
  (9, 'c15hour', 'du','Contacts par minute (15s)'),
  (10, 'c5night', 'du','Contacts par nuit (5s)'),
  (11, 'c15night', 'du','Contacts par nuit (15s)');


INSERT INTO dicts_biomtesticule VALUES
  (0, 't0', 'invisibles', 'testicules invisibles'),
  (1, 't1', 'gonflés', 'testicules gonflés'),
  (2, 't2', 'très gonflés', 'testicules très gonflés');

INSERT INTO dicts_biomepipidyme VALUES
  (0, 'e0', 'non gonflés', 'épididymes non gonflés'),
  (1, 'e1', 'gonflés', 'épididymes gonflés'),
  (2, 'e2', 'très gonflés', 'épididymes très gonflés');


INSERT INTO dicts_biomtuniqvag VALUES
  (0, 's', 'sombres, noires', 'tuniques vaginales sombres, noires'),
  (1, 'b', 'bicolores', 'tuniques vaginales bicolores'),
  (2, 'c', 'claires, diffuses', 'tuniques vaginales claires, diffuses');

INSERT INTO dicts_biomglandtaille VALUES
  (0, 'gd0', 'non visibles', 'glandes buccales non visibles'),
  (1, 'gd1', 'apparentes', 'glandes buccales apparentes'),
  (2, 'gd2', 'proéminentes', 'glandes buccales proéminentes');

INSERT INTO dicts_biomglandcoul VALUES
  (0, 'p', 'roses', 'glandes buccales roses'),
  (1, 'w', 'blanches', 'glandes buccales blanches'),
  (2, 'or', 'oranges', 'glandes buccales oranges');

INSERT INTO dicts_biommamelle VALUES
  (0, 'm0', 'invisibles ou très petites',
   'mamelles invisibles ou très petites ; poils sur les mamelles et pourtours identiques au reste du pelage'),
  (1, 'm1', 'visibles', 'mamelles visibles, non gonflées ; poils longs mais plus fins sur mamelles et pourtours'),
  (2, 'm2', 'gonflées', 'mamelles gonflées, tendues, machouillées, présence de lait ; mamelles et pourtours dénudés'),
  (3, 'm3', 'visibles, non gonflées',
   'mamelles visibles, non gonflées ; mamelles et pourtours recouverts de poils très courts (repousse)');

INSERT INTO dicts_biomgestation VALUES
  (1, 'ng', 'probablement non gestante', 'probablement non gestante'),
  (2, 'pg', 'probablement gestante', 'probablement gestante'),
  (3, 'g', 'gestante', 'gestante (palpée)');

INSERT INTO dicts_biomepiphyse VALUES
  (0, 'f0', 'pas de fusion', 'pas de fusion ; articulation rectiligne, alternance de zones claires et sombres'),
  (1, 'f1', 'fusion en cours', 'fusion en cours ; articulation enflée, zones claires encore visibles'),
  (2, 'f2', 'fusion terminé', 'fusion terminée ; articulation noduleuse, pas de zones claires');

INSERT INTO dicts_biomchinspot (id, code, short_descr, descr) VALUES
  (0, 'c0', 'non visible', 'chinspot non visible'),
  (1, 'c1', 'peu visible', 'chinspot peu visible'),
  (2, 'c2', 'bien visible', 'chinspot bien visible');

INSERT INTO dicts_biomdent (id, code, short_descr, descr) VALUES
  (0, 'u0', 'très pointues', 'dents très pointues'),
  (1, 'u1', 'pointues', 'dents pointues'),
  (2, 'u2', 'limées', 'dents limées');


INSERT INTO dicts_state (id, code) VALUES
  (1, 'mauvais'),
  (2, 'moyen'),
  (3, 'bon'),
  (4, 'neuf');

INSERT INTO dicts_interest (id, code) VALUES
  (1, 'nul'),
  (2, 'moyen'),
  (3, 'bon'),
  (4, 'excellent');

INSERT INTO dicts_age (id, code, descr) VALUES
  (1, 'j', 'juvénile'),
  (2, 'sad', 'subadulte'),
  (3, 'ad', 'adulte'),
  (4, 'aj', 'adulte et juvenile'),
  (5, 'v', 'vieux');


INSERT INTO dicts_sex (id, code, descr) VALUES
  (1, 'f', 'Femelle'),
  (2, 'm', 'Mâle'),
  (3, 'fm', 'Femelle et male'),
  (4, 'nd', 'Non déterminé');

INSERT INTO dicts_period (id, code, descr, period, day_start, month_start, day_end, month_end) VALUES
  ('1', 'h', 'Hibernant', '01/12 au 28(29)/02', '1', '12', '29', '2'),
  ('2', 'tp', 'Transit printanier', '01/03 au 15/05', '1', '3', '15', '5'),
  ('3', 'e', 'Estivant', '16/05 au 15/08', '16', '5', '15', '8'),
  ('4', 'ta', 'Transit automnal', '16/08 au 30/11', '16', '8', '30', '11');

INSERT INTO dicts_treehealth (id, descr) VALUES
  (1, 'Vivant'),
  (2, 'Mort');

INSERT INTO dicts_treecircumstance (id, code, descr) VALUES
  (1, 'calls', 'Cris audibles (révélant l''emplacement du gîte'),
  (2, 'out', 'Sortie de gîte supposée'),
  (3, 'radtrk', 'Radiopistage'),
  (4, 'search', 'Prospection dans les arbres'),
  (5, 'prun', 'Travaux d''élagage'),
  (6, 'view', 'Observation visuelle');

INSERT INTO dicts_treebecoming (id, code, descr) VALUES
  (1, 'prot', 'Arbre protégé'),
  (2, 'inplac', 'Arbre toujours en place'),
  (3, 'felpro', 'Abattage de l''arbre programmé'),
  (4, 'felled', 'Arbre abattu'),
  (5, 'natfel', 'Tombé naturellement'),
  (6, 'nd', 'Non défini');

INSERT INTO dicts_treeforeststands (id, code, descr) VALUES
  (1, 'for', 'Futaie'),
  (2, 'copfor', 'Taillis sous futaie'),
  (3, 'cop', 'Taillis');

INSERT INTO dicts_treecontext (id, code, descr) VALUES
  (1, 'for', 'Forêt'),
  (2, 'gar', 'Jardin'),
  (3, 'sha', 'Alignement d''arbres'),
  (4, 'hed', 'Talus ou haie boisée'),
  (5, 'orc', 'Verger');


INSERT INTO dicts_treegiteplace (id, code, descr) VALUES
  (1, 'trunk', 'Tronc'),
  (2, 'branc', 'Branche'),
  (3, 'carp', 'Charpentière');

INSERT INTO dicts_treegitetype (id, code, descr) VALUES
  (1, 'bir', 'Trou de pic'),
  (2, 'bircom', 'Trou de pic communiquants'),
  (3, 'cranar', 'Fissure étroite (< 3 cm)'),
  (4, 'crawid', 'Fissure large (> 3 cm)'),
  (5, 'brains', 'Insertion (creuse) de branche'),
  (6, 'truins', 'Insertion (creuse) de tronc'),
  (7, 'decay', 'Carie'),
  (8, 'bark', 'Sous l''écorce'),
  (9, 'clipla', 'Derrière le lierre'),
  (10, 'truspl', 'Blessure en forme d''écharde sur le tronc'),
  (11, 'braspl', 'Trou sous ou sur une ancienne branche tombée');

INSERT INTO dicts_treegiteorigin (id, code, descr) VALUES
  (1, 'clim', 'Climatique'),
  (2, 'forexp', 'Exploitation forestière'),
  (3, 'prun', 'Travaux d''élagage'),
  (4, 'biol', 'Biologique'),
  (5, 'fire', 'Feu'),
  (6, 'lightn', 'Foudre'),
  (7, 'bird', 'Trou de pic'),
  (8, 'insect', 'Trou de gros insecte');

insert into dicts_placemanagementaction(id, code, category, label) VALUES
  (1,'am','-','Aménagements Chiroptères'),
  (2,'cl','-','Classement en espace protégé'),
  (3,'co','-','Convention'),
  (4,'gm','-','Gestion des milieux naturels'),
  (5,'in','-','Informel'),
  (6,'mf','-','Maîtrise foncière'),
  (7,'pi','-','Panneau d''information'),
  (8,'pp','-','Protection physique'),
  (9,'xx','-','Autres (préciser en commentaires)');


