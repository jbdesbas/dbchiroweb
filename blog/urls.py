from django.conf.urls import url

from .views import ActuCreate, ActuUpdate, ActuDelete, ActuDetail, ActuList

urlpatterns = [
    url(r'^$', ActuList.as_view(), name='home'),
    url(r'^actu/list/page(?P<page>[0-9]+)/$', ActuList.as_view(), name='actu_list'),
    url(r'^actu/(?P<pk>\d+)/detail$', ActuDetail.as_view(), name='actu_detail'),
    url(r'^actu/add$', ActuCreate.as_view(), name='actu_create'),
    url(r'^actu/(?P<pk>[0-9]+)/update$',
        ActuUpdate.as_view(), name='actu_update'),
    url(r'^actu/(?P<pk>[0-9]+)/delete$',
        ActuDelete.as_view(), name='actu_delete'),

]
