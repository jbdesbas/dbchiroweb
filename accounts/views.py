from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.mail import send_mail
from django.shortcuts import render
from django.urls import reverse
from django.urls import reverse_lazy
from django.utils.translation import ugettext_lazy as _
from django.views.generic import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django_filters.views import FilterView
from django_tables2 import SingleTableView

from core.functions import generate_username
from .filters import ProfileFilter
from .forms import (
    UserCreateAdminForm,
    UserCreateForm,
    UserAdminUpdatePasswordForm,
    PasswordUpdateForm,
    UserUpdateAdminForm,
    UserUpdateForm,
)
from .mixins import ManageAccountAuthMixin, ManageMyAccountAuthMixin
from .models import Profile
from .tables import ProfileTable


class UserCreate(ManageAccountAuthMixin, CreateView):
    """
    User Creation View
    """

    model = Profile
    template_name = "leaflet_form.html"

    def get_form_class(self):
        user = self.request.user
        if user.edit_all_data or user.is_superuser:
            return UserCreateAdminForm
        else:
            return UserCreateForm

    def get_initial(self):
        initial = super(UserCreate, self).get_initial()
        initial = initial.copy()
        pwdgen = Profile.objects.make_random_password()
        initial["password_clear"] = pwdgen
        initial["password1"] = pwdgen
        initial["password2"] = pwdgen
        return initial

    def form_valid(self, form):
        form.instance.created_by = self.request.user.username
        form.instance.is_active = True
        form.instance.username = generate_username(
            form.instance.first_name, form.instance.last_name
        )
        # TODO : Email automatique à la création d'un nouveau compte avec identifiant et mot de passe
        form.instance.password1 = form.cleaned_data["password_clear"]
        form.instance.password2 = form.cleaned_data["password_clear"]
        emailinfo = form.save(commit=False)
        subject = _("Création de votre compte %s") % (emailinfo.username)
        system_sender = settings.DEFAULT_FROM_EMAIL
        real_sender = self.request.user.email
        txt_content = ("Identifiant: %s | mdp %s") % (
            emailinfo.username,
            form.cleaned_data["password_clear"],
        )
        html_content = (
            '<h1>Votre nouveau compte dbChiro.org</h1><p><b>Identifiant</b> : %s </p><p><b>Mot de passe</b> : %s </p><p><a href="http://%s">Connectez-vous ici</a></p><p>--<br/>%s<br/>%s</p>'
        ) % (
            emailinfo.username,
            form.cleaned_data["password_clear"],
            self.request.get_host(),
            self.request.user.get_full_name(),
            self.request.user.email,
        )
        receipter = [emailinfo.email, system_sender, real_sender]
        send_mail(
            subject,
            txt_content,
            system_sender,
            receipter,
            fail_silently=True,
            html_message=html_content,
        )
        form.save()
        return super(UserCreate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(UserCreate, self).get_context_data(**kwargs)
        context["icon"] = "fi-torsos"
        context["title"] = "Création d'un compte"
        context[
            "js"
        ] = """
        """
        return context

        # def get_success_url(self):
        #     return reverse_lazy('user_search')


class UserUpdate(ManageAccountAuthMixin, UpdateView):
    """
    User Creation View
    """

    model = Profile
    template_name = "leaflet_form.html"

    def get_form_class(self):
        user = self.request.user
        if user.edit_all_data or user.is_superuser:
            return UserUpdateAdminForm
        else:
            return UserUpdateForm

    def get_form_kwargs(self):
        kwargs = super(UserUpdate, self).get_form_kwargs()
        user = self.request.user
        return kwargs

    def form_valid(self, form):
        form.instance.updated_by = self.request.user.username
        form.instance.is_active = True
        return super(UserUpdate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(UserUpdate, self).get_context_data(**kwargs)
        context["icon"] = "fi-torsos"
        context["title"] = "Modification d'un compte"
        return context


class UserPassword(ManageAccountAuthMixin, UpdateView):
    model = Profile
    form_class = UserAdminUpdatePasswordForm
    success_url = reverse_lazy("password_reset_complete")
    template_name = "normal_form.html"
    title = _("Enter new password")

    def get_form_kwargs(self):
        kwargs = super(UserPassword, self).get_form_kwargs()
        username = self.object.username
        print(username)
        return kwargs

    def get_success_url(self):
        return reverse("accounts:user_detail", kwargs={"pk": self.object.pk})

    def form_valid(self, form):
        form.instance.updated_by = self.request.user.username
        form.instance.is_active = True
        return super(UserPassword, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(UserPassword, self).get_context_data(**kwargs)
        context["title"] = _("Changement du mot de passe")
        context["icon"] = "fi-lock"
        context[
            "js"
        ] = """
        """
        return context


class MyProfileUpdate(ManageMyAccountAuthMixin, UpdateView):
    """
    User Creation View
    """

    model = Profile
    form_class = UserUpdateForm
    template_name = "leaflet_form.html"

    def form_valid(self, form):
        form.instance.updated_by = self.request.user.username
        return super(MyProfileUpdate, self).form_valid(form)

    def get_object(self):
        return get_user_model().objects.get(
            id=self.request.user.id
        )  # or request.POST

    def get_context_data(self, **kwargs):
        context = super(MyProfileUpdate, self).get_context_data(**kwargs)
        context["icon"] = "fi-torsos"
        context["title"] = _("Modification de mon compte")
        return context


class UserDelete(ManageAccountAuthMixin, DeleteView):
    model = Profile
    template_name = "confirm_delete.html"
    success_url = reverse_lazy("accounts:user_search")

    def get_context_data(self, **kwargs):
        context = super(UserDelete, self).get_context_data(**kwargs)
        context["icon"] = "fi-trash"
        context["title"] = "Suppression d'un compte"
        context["message_alert"] = _(
            "Êtes-vous certain de vouloir supprimer l'utilisateur"
        )
        return context


class UserDetail(LoginRequiredMixin, DetailView):
    model = get_user_model()
    template_name = "accounts/profile.html"


class MyProfileDetail(LoginRequiredMixin, DetailView):
    model = get_user_model()
    template_name = "accounts/profile.html"

    def get_object(self):
        return get_user_model().objects.get(
            id=self.request.user.id
        )  # or request.POST


@login_required()
def change_password(request):
    """
    Password update view
    :param request:
    :return:
    """
    form = PasswordUpdateForm(user=request.user)
    title = _("Modifier son mot de passe")
    icon = "fi-lock"
    js = ""
    if request.method == "POST":
        form = PasswordUpdateForm(user=request.user, data=request.POST)
        if form.is_valid():
            form.save()
            update_session_auth_hash(request, form.user)

    return render(
        request,
        "normal_form.html",
        {"form": form, "title": title, "icon": icon, "js": js},
    )


class UserListView(ManageAccountAuthMixin, FilterView, SingleTableView):
    table_class = ProfileTable
    model = Profile
    template_name = "table.html"

    filterset_class = ProfileFilter

    def get_queryset(self):
        """Returns accounts ordered by update timestamp"""
        return Profile.objects.all().order_by("-last_login")

    def get_context_data(self, **kwargs):
        context = super(UserListView, self).get_context_data(**kwargs)
        context["icon"] = "fi-torsos-all-female"
        context["title"] = _("Rechercher un observateur")
        context[
            "js"
        ] = """
        """
        return context
