import django_tables2 as tables
from django.utils.translation import ugettext_lazy as _
from django_tables2.utils import A

from .models import Profile


class ProfileTable(tables.Table):
    ACTIONS = '''
            <div class="btn-group btn-group-xs">
                <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                    Action <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a data-toggle="modal"
                           data-target=".bs-user-history-modal-md-{{ record.pk }}" title="Historique de la donnée"><i
                            class="fa fa-fw fa-history"></i> Historique de la donnée</a></li>
                    <li><a href="{% url 'accounts:user_detail' record.pk %}" title="Fiche détaillée"><i class="fa fa-fw fa-info-circle" aria-hidden="true"></i> Fiche
                        détaillée</a></li>
                    <li><a href="{% url 'accounts:user_update' record.pk %}" title="Modifier"><i class="fa fa-fw fa-pencil-square-o"></i> Modifier le profil</a>
                    </li>
                    <li><a href="{% url 'accounts:user_password' record.pk %}" title="Modifier"><i class="fa fa-fw fa-key"></i> Modifier le mot de passe</a>
                    </li>
                    <li><a href="{% url 'sights:user_sighting_list' record.pk %}" Title="Voir ses observations"><i
                            class="fa fa-fw fa-eye"></i> Ses observations</a></li>
                    <li role="separator" class="divider"></li>
                    <li style="color:red;"><a href="{% url 'accounts:user_delete' record.pk %}" title="Supprimer"><i
                            class="fa fa-fw fa-trash"></i> Supprimer</a></li>
                </ul>
            </div>
            <div class="modal fade bs-user-history-modal-md-{{ record.pk }}" tabindex="-1" role="dialog"
                 aria-labelledby="history">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-fw fa-history"></i> Historique</h4>
                        </div>
                        <div class="modal-body">
                            <p>Donnée créée le {{ record.timestamp_create }} par {{ record.created_by }}
                                {% if record.timestamp_update %}
                                    et mise à jour le {{ record.timestamp_update }}
                                {% endif %}
                                par {{ record.updated_by }}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            '''
    MODIFY_BY = """
    {% if record.updated_by %}{{record.updated_by}}{% else %}{{ record.created_by }}{% endif %}
    """
    is_resp = tables.BooleanColumn(verbose_name=_('Responsable local'))
    access_all_data = tables.BooleanColumn(verbose_name=_('Peut tout lire'))
    edit_all_data = tables.BooleanColumn(verbose_name=_('Peut tout éditer'))
    mobile_phone = tables.Column(verbose_name=_('n° mobile'))
    home_phone = tables.Column(verbose_name=_('n° domicile'))
    email = tables.EmailColumn(verbose_name=_('@email'))
    actions = tables.TemplateColumn(
        ACTIONS, verbose_name="Actions", orderable=False, attrs={'th': {'style': 'width: 75px;'}})
    username = tables.LinkColumn('accounts:user_detail', args=[A('id')], accessor='username')
    modify_by = tables.TemplateColumn(
        MODIFY_BY, verbose_name="Dernière modification", orderable=False)
    last_login = tables.DateTimeColumn()

    class Meta:
        model = Profile
        template = 'table_bootstrap.html'
        attrs = {'class': 'table table-striped'}
        fields = (
            'actions', 'username', 'email', 'first_name', 'last_name', 'organism', 'is_active', 'is_resp',
            'access_all_data',
            'edit_all_data', 'home_phone', 'mobile_phone', 'addr_city', 'last_login', 'modify_by',)
        exclude = ('id',)
