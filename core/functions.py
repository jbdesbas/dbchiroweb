import re

from django.conf import settings
from django.contrib.auth import get_user_model
from django.template.defaultfilters import slugify


def get_sight_period(dateobs):
    doy = dateobs.timetuple().tm_yday
    w = int(settings.PERIOD_WINTERING_START)
    st = int(settings.PERIOD_SPRING_TRANSIT_START)
    e = int(settings.PERIOD_SUMMERING_START)
    at = int(settings.PERIOD_AUTUMN_TRANSIT_START)
    if doy <= st or doy > w:
        return settings.PERIOD_WINTERING_VALUE
    elif doy > st and doy <= e:
        return settings.PERIOD_SPRING_TRANSIT_VALUE
    elif doy > e and doy <= at:
        return settings.PERIOD_SUMMERING_VALUE
    elif doy > at and doy <= w:
        return settings.PERIOD_AUTUMN_TRANSIT_VALUE


def generate_username(first_name, last_name):
    fnl = first_name.split('-')
    fni = ""
    for name in fnl:
        fni = fni + name[0]
    val = "{0}{1}".format(fni, last_name).lower()
    val = re.sub('[!\' -@#$%,]', '', val)
    val = slugify(val)
    x = 0
    while True:
        if x == 0 and get_user_model().objects.filter(username=val).count() == 0:
            return val
        else:
            x += 1
            new_val = "{0}{1}".format(val, x)
            if get_user_model().objects.filter(username=new_val).count() == 0:
                return new_val
        if x > 1000000:
            raise Exception("Name is super popular!")
