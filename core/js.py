DateAndTimeInput = """
        $(function () {
            $('.dateinput').datetimepicker({
                useCurrent: false,
                format: 'DD/MM/YYYY',
                maxDate: Date.now(),
                showTodayButton: true,
                showClear: true,
                showClose: true
            });
            $('.timeinput').datetimepicker({
                useCurrent: false,
                defaultDate:false,
                format: 'HH:mm',
            });
        });
"""

DateInput = """
        $(function () {
            $('.dateinput').datetimepicker({
                useCurrent: false,
                format: 'DD/MM/YYYY',
                maxDate: Date.now(),
                showTodayButton: true,
                showClear: true,
                showClose: true
            });
        });
"""


TimeInput = """
        $(function () {
            $('.timeinput').datetimepicker({
                format: 'HH:mm'
            });
        });
"""