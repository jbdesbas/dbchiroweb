"""
	Mixins
"""

from django.contrib.auth.mixins import LoginRequiredMixin


class ManageAccountAuthMixin:
    '''
    Classe mixin de vérification que l'utilisateur possède les droits de créer le compte
    '''

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
        else:
            loggeduser = get_user_model().objects.get(id=request.user.id)
            if loggeduser.edit_all_data or loggeduser.access_all_data or loggeduser.is_resp or loggeduser.is_staff or loggeduser.is_superuser:
                return super(ManageAccountAuthMixin, self).dispatch(request, *args, **kwargs)
            else:
                return redirect('core:view_unauth')

