from django.conf import settings
from django.db import models
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _

from geodata.models import Territory


class CatchAuth(models.Model):
    id_catchauth = models.AutoField(primary_key=True)
    territory = models.ManyToManyField(
        Territory,
        blank=True,
        verbose_name=_("Couverture territoriale"),
        related_name="catchauth",
    )
    date_start = models.DateField(
        verbose_name=_("Date de début d'application"),
        help_text=_("format jj/mm/aaaa"),
    )
    date_end = models.DateField(
        verbose_name=_("Date de fin d'application"),
        help_text=_("format jj/mm/aaaa"),
    )
    official_reference = models.CharField(
        max_length=255,
        unique=True,
        verbose_name=_("Référence du texte officiel"),
    )
    file = models.FileField(
        upload_to="catchauth/%Y/",
        blank=True,
        null=True,
        verbose_name=_("Texte officiel"),
    )
    timestamp_create = models.DateTimeField(auto_now_add=True, editable=False)
    timestamp_update = models.DateTimeField(auto_now=True, editable=False)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        db_index=True,
        editable=False,
        related_name="catchauth_creator",
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        db_index=True,
        editable=False,
        related_name="catchauth_modifier",
    )

    def __str__(self):
        return "Autorisation %s du %s au %s" % (
            self.official_reference,
            self.date_start,
            self.date_end,
        )

    def get_absolute_url(self):
        return reverse("management:catchauth_list")

    class Meta:
        verbose_name = "Autorisation de manipulation / capture"
        verbose_name_plural = "Autorisations de manipulation / capture"


class Study(models.Model):
    id_study = models.AutoField(primary_key=True, db_index=True)
    name = models.CharField(
        max_length=255, db_index=True, verbose_name="nom du l'étude"
    )
    year = models.CharField(
        max_length=10,
        db_index=True,
        verbose_name="Année de réalisation l'étude",
    )
    project_manager = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        db_index=True,
        verbose_name="Responsable de l'étude",
        related_name="study",
        null=True
    )
    #     study_organism = models.ForeignKey(
    #         'Organism',
    #         models.DO_NOTHING,
    #         db_column='name',
    #         blank=True,
    #         null=True)
    #     study_sponsor = models.ForeignKey(
    #         'Organism',
    #         models.DO_NOTHING,
    #         db_column='name',
    #         blank=True,
    #         null=True)
    public_funding = models.BooleanField(verbose_name="Financement publique")
    public_report = models.BooleanField(verbose_name="Rapport rendu publique")
    public_raw_data = models.BooleanField(verbose_name="Donnée brute publique")
    confidential = models.BooleanField(verbose_name="Confidentiel")
    confidential_end_date = models.DateField(
        blank=True, null=True, verbose_name="Date de fin de la confidentialité"
    )
    type_etude = models.CharField(max_length=255, verbose_name="Type d'étude")
    type_espace = models.CharField(
        max_length=255, blank=True, null=True, verbose_name="Type d'espace"
    )
    comment = models.TextField(
        blank=True, null=True, verbose_name="Commentaire"
    )
    timestamp_create = models.DateTimeField(auto_now_add=True, editable=False)
    timestamp_update = models.DateTimeField(auto_now=True, editable=False)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        db_index=True,
        editable=False,
        related_name="study_creator",
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        db_index=True,
        editable=False,
        related_name="study_modifier",
    )

    def __str__(self):
        return "%s (%s)" % (self.name, self.year)

    def get_absolute_url(self):
        return reverse("management:study_list")

    class Meta:
        verbose_name = "Etude"
        verbose_name_plural = "Etudes"
        indexes = [
            models.Index(
                fields=["-timestamp_update"], name="etude_timestamp_update_idx"
            ),
            models.Index(fields=["name"], name="etude_name_idx"),
            models.Index(fields=["created_by"], name="etude_created_by_idx"),
        ]
        unique_together = ["name", "year"]
        ordering = ["-timestamp_create"]


class Transmitter(models.Model):
    TRANSMITTER_STATUS = [
        ("ok", "En stock et opérationnel"),
        ("low", "Batterie vide/déchargée"),
        ("lost", "Perdu"),
        ("out", "hors-service"),
    ]
    id_transmitter = models.AutoField(primary_key=True, verbose_name="ID")
    name = models.CharField(
        max_length=150, null=True, blank=True, verbose_name="Nom unique"
    )
    reference = models.CharField(
        max_length=30, unique=True, null=True, verbose_name="Référence"
    )
    frequency = models.FloatField(null=True, verbose_name="Fréquence en MHz")
    weight = models.FloatField(
        null=True, blank=True, verbose_name="Poids (en grammes)"
    )
    autonomy = models.FloatField(
        null=True, blank=True, verbose_name="Autonomie (en jours)"
    )
    model = models.CharField(
        max_length=100, null=True, blank=True, verbose_name="Modèle"
    )
    brand = models.CharField(
        max_length=100, null=True, blank=True, verbose_name="Marque"
    )
    owner = models.CharField(
        max_length=100, null=True, blank=True, verbose_name="Propriétaire"
    )
    buying_date = models.DateField(
        null=True, blank=True, verbose_name="Date d'achat"
    )
    last_recond_date = models.DateField(
        null=True, blank=True, verbose_name="Date de dernier reconditionnement"
    )
    status = models.CharField(
        max_length=50,
        choices=TRANSMITTER_STATUS,
        default="ok",
        verbose_name="Statut",
    )
    available = models.BooleanField(
        verbose_name="Disponible pour la saisie", default=True
    )
    comment = models.TextField(
        blank=True, null=True, verbose_name="Commentaire"
    )
    timestamp_create = models.DateTimeField(auto_now_add=True, editable=False)
    timestamp_update = models.DateTimeField(auto_now=True, editable=False)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        db_index=True,
        editable=False,
        related_name="transmitter_creator",
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        db_index=True,
        editable=False,
        related_name="transmitter_modifier",
    )

    def __str__(self):
        return "n°%s ∙ %s MHz" % (self.reference, self.frequency)

    def get_absolute_url(self):
        return reverse("management:transmitter_list")

    class Meta:
        verbose_name = "Emetteur posé"
        verbose_name_plural = "Emetteurs posés"
        unique_together = ["reference", "frequency"]
