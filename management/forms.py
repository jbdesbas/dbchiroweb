from crispy_forms.bootstrap import Accordion, Div
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Fieldset, Column, Layout, Row, Submit, Button
from dal import autocomplete
from django import forms
from django.utils.translation import ugettext_lazy as _

from core.forms import InfoAccordionGroup, PrimaryAccordionGroup
from .models import Study, Transmitter, CatchAuth


class StudyForm(forms.ModelForm):
    class Meta:
        model = Study
        fields = (
            "name",
            "year",
            "project_manager",
            "public_funding",
            "public_report",
            "public_raw_data",
            "confidential",
            "confidential_end_date",
            "type_etude",
            "type_espace",
            "comment",
        )
        widgets = {
            "project_manager": autocomplete.ModelSelect2(
                url="api:all_user_autocomplete"
            )
        }

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.form_action = "submit"
        # ======================================================================
        # self.fields['transmitter'].queryset = Transmitter.objects.filter(
        #     available is True)
        # ======================================================================
        self.helper.layout = Layout(
            Row(
                Column(
                    Submit(
                        "submit",
                        _("Enregistrer"),
                        css_class="btn-primary btn-sm",
                    ),
                    Button(
                        "cancel",
                        _("Retour"),
                        css_class="btn-warning btn-sm",
                        onclick="history.go(-1)",
                    ),
                    css_class="col-lg-12 btn-group right",
                    role="button",
                )
            ),
            Accordion(
                PrimaryAccordionGroup(
                    "Informations principales",
                    Row(
                        Column(
                            Fieldset(
                                _("Caractéristiques principales"),
                                Column("name", css_class="col-lg-4 col-xs-12"),
                                Column("year", css_class="col-lg-4 col-xs-6"),
                                Column(
                                    "project_manager",
                                    css_class="col-lg-4 col-xs-6",
                                ),
                                Column(
                                    "type_etude", css_class="col-lg-6 col-xs-12"
                                ),
                                Column(
                                    "type_espace",
                                    css_class="col-lg-6 col-xs-12",
                                ),
                                css_class="col-lg-12",
                            ),
                            Fieldset(
                                _("Caractéristiques principales"),
                                Column(
                                    "public_funding",
                                    css_class="col-lg-4 col-xs-6",
                                ),
                                Column(
                                    "public_report",
                                    css_class="col-lg-4 col-xs-6",
                                ),
                                Column(
                                    "public_raw_data",
                                    css_class="col-lg-4 col-xs-6",
                                ),
                                Column(
                                    "confidential",
                                    css_class="col-lg-6 col-xs-6",
                                ),
                                Column(
                                    "confidential_end_date",
                                    css_class="col-lg-6 col-xs-6",
                                ),
                                css_class="col-lg-12",
                            ),
                        )
                    ),
                ),
                InfoAccordionGroup(
                    "Commentaire", Row(Column("comment", css_class="col-lg-12"))
                ),
            ),
            Row(
                Column(
                    Submit(
                        "submit",
                        _("Enregistrer"),
                        css_class="btn-primary btn-sm",
                    ),
                    Button(
                        "cancel",
                        _("Retour"),
                        css_class="btn-warning btn-sm",
                        onclick="history.go(-1)",
                    ),
                    css_class="col-lg-12 btn-group right",
                    role="button",
                )
            ),
        )

        super(StudyForm, self).__init__(*args, **kwargs)


class TransmitterForm(forms.ModelForm):
    class Meta:
        model = Transmitter
        fields = (
            "reference",
            "frequency",
            "available",
            "weight",
            "autonomy",
            "model",
            "brand",
            "owner",
            "buying_date",
            "last_recond_date",
            "status",
            "comment",
        )

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.form_action = "submit"
        self.helper.layout = Layout(
            Row(
                Column(
                    Submit(
                        "submit",
                        _("Enregistrer"),
                        css_class="btn-primary btn-sm",
                    ),
                    Button(
                        "cancel",
                        _("Retour"),
                        css_class="btn-warning btn-sm",
                        onclick="history.go(-1)",
                    ),
                    css_class="col-lg-12 btn-group right",
                    role="button",
                )
            ),
            Div(
                Div("available", css_class="panel-body"),
                css_class="panel panel-default",
            ),
            Accordion(
                PrimaryAccordionGroup(
                    _("Informations principales"),
                    Row(
                        Column(
                            "reference", css_class="col-lg-4 col-md-6 col-xs-12"
                        ),
                        Column(
                            "frequency", css_class="col-lg-4 col-md-6 col-xs-12"
                        ),
                        Column(
                            "weight", css_class="col-lg-4 col-md-6 col-xs-12"
                        ),
                        Column(
                            "autonomy", css_class="col-lg-4 col-md-6 col-xs-12"
                        ),
                        Column(
                            "model", css_class="col-lg-4 col-md-6 col-xs-12"
                        ),
                        Column(
                            "brand", css_class="col-lg-4 col-md-6 col-xs-12"
                        ),
                        Column(
                            "buying_date",
                            css_class="col-lg-6 col-md-6 col-xs-12",
                        ),
                        Column(
                            "last_recond_date",
                            css_class="col-lg-6 col-md-6 col-xs-12",
                        ),
                    ),
                    Row(
                        Column("owner", css_class="col-md-6 col-xs-12"),
                        Column("status", css_class="col-md-6 col-xs-12"),
                    ),
                ),
                InfoAccordionGroup(
                    "Commentaire", Row(Column("comment", css_class="col-lg-12"))
                ),
            ),
            Row(
                Column(
                    Submit(
                        "submit",
                        _("Enregistrer"),
                        css_class="btn-primary btn-sm",
                    ),
                    Button(
                        "cancel",
                        _("Retour"),
                        css_class="btn-warning btn-sm",
                        onclick="history.go(-1)",
                    ),
                    css_class="col-lg-12 btn-group right",
                    role="button",
                )
            ),
        )

        super(TransmitterForm, self).__init__(*args, **kwargs)


class CatchAuthForm(forms.ModelForm):
    class Meta:
        model = CatchAuth
        fields = (
            "territory",
            "date_start",
            "date_end",
            "official_reference",
            "file",
        )
        widgets = {
            "territory": autocomplete.ModelSelect2Multiple(
                url="api:territory_autocomplete"
            )
        }

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.form_action = "submit"
        # ======================================================================
        # self.fields['transmitter'].queryset = Transmitter.objects.filter(
        #     available is True)
        # ======================================================================
        self.helper.layout = Layout(
            Row(
                Column(
                    Submit(
                        "submit",
                        _("Enregistrer"),
                        css_class="btn-primary btn-sm",
                    ),
                    Button(
                        "cancel",
                        _("Retour"),
                        css_class="btn-warning btn-sm",
                        onclick="history.go(-1)",
                    ),
                    css_class="col-lg-12 btn-group right",
                    role="button",
                )
            ),
            Accordion(
                PrimaryAccordionGroup(
                    "Données de l'arrêté",
                    Row(
                        Column("territory", css_class="col-lg-6 col-xs-12"),
                        Column(
                            "official_reference", css_class="col-lg-6 col-xs-12"
                        ),
                        Column("date_start", css_class="col-lg-6 col-xs-12"),
                        Column("date_end", css_class="col-lg-6 col-xs-12"),
                    ),
                ),
                InfoAccordionGroup(
                    _("Fichier"), Row(Column("file", css_class="col-lg-12"))
                ),
            ),
            Row(
                Column(
                    Submit(
                        "submit",
                        _("Enregistrer"),
                        css_class="btn-primary btn-sm",
                    ),
                    Button(
                        "cancel",
                        _("Retour"),
                        css_class="btn-warning btn-sm",
                        onclick="history.go(-1)",
                    ),
                    css_class="col-lg-12 btn-group right",
                    role="button",
                )
            ),
        )

        super(CatchAuthForm, self).__init__(*args, **kwargs)
