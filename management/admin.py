from django.contrib import admin
from .models import CatchAuth, Study

class CatchAuthAdmin(admin.ModelAdmin):
    model = CatchAuth
    list_display = ('id_catchauth', 'date_start', 'date_end',
                    'official_reference', 'file')
    search_fields = ('territory',)
    sortable_field_name = ['__all__']

# Register your models here.
admin.site.register(CatchAuth, CatchAuthAdmin)
