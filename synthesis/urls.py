from django.conf.urls import url

from .views import GlobalContribView

urlpatterns = [
    # url de recherches de localités
    url(r'^contrib$', GlobalContribView.as_view(), name='global_contrib'),
]
