"""
	Mixins du projet
"""

from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import redirect


class ModifyDataAuthMixin(LoginRequiredMixin):
    """
    Classe mixin de vérification que l'utilisateur possède les droits de modifier la donnée:
    Soit il est créateur de la donnée,
    Soit il dispose de l'authorisation de modifier toutes les données ( :get_user_model():access_all_data is True)
    """

    def dispatch(self, request, *args, **kwargs):
        self.obj = self.get_object()
        if not request.user.is_authenticated:
            return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
        if get_user_model().objects.get(id=request.user.id).filter(
                edit_all_data=True) or self.obj.created_by == request.user:
            print('access granted')
            return super(ModifyDataAuthMixin, self).dispatch(request, *args, **kwargs)
        else:
            return redirect('core:update_unauth')


class AdvancedUserViewMixin(LoginRequiredMixin):
    """
    Classe mixin de vérification que l'utilisateur possède les droits de modifier la donnée:
    Soit il est créateur de la donnée,
    Soit il dispose de l'authorisation de modifier toutes les données ( :get_user_model():access_all_data is True)
    """

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
        user = get_user_model().objects.get(id=request.user.id)
        if user.edit_all_data or user.access_all_data or user.is_resp or user.is_superuser:
            print('access granted')
            return super(AdvancedUserViewMixin, self).dispatch(request, *args, **kwargs)
        else:
            return redirect('core:update_unauth')


class PlaceViewAuthMixin(LoginRequiredMixin):
    '''
    Classe mixin de vérification que l'utilisateur possède les droits de modifier la donnée:
    Soit il est créateur de la donnée,
    Soit il dispose de l'authorisation de modifier toutes les données ( :get_user_model():access_all_data is True)
    '''

    def dispatch(self, request, *args, **kwargs):
        self.obj = self.get_object()
        if not request.user.is_authenticated:
            return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
        else:
            loggeduser = get_user_model().objects.get(id=request.user.id)
            if loggeduser.edit_all_data \
                    or self.obj.created_by == loggeduser \
                    or (self.obj.is_hidden and loggeduser in self.obj.authorized_user.all()) \
                    or (loggeduser.is_resp and self.obj.territory in loggeduser.resp_territory.all()) \
                    or not self.obj.is_hidden:
                print('access granted')
                return super(PlaceViewAuthMixin, self).dispatch(request, *args, **kwargs)
            else:
                return redirect('core:view_unauth')


class PlaceEditAuthMixin:
    """
    Permission d'éditer la localité dans les conditions suivantes:
        L'utilisateur est superutilisateur,
        L'utilisateur peut modifier Toutes les données,
        L'utilisateur est responsable du territoire de la localité,
        L'utilisateur est créateur de la localité.
    """

    def dispatch(self, request, *args, **kwargs):
        self.obj = self.get_object()
        if not request.user.is_authenticated:
            return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
        else:
            loggeduser = get_user_model().objects.get(id=request.user.id)
            if loggeduser.is_superuser or loggeduser.edit_all_data or (
                    loggeduser.is_resp and self.obj.territory in loggeduser.resp_territory.all()) or self.obj.created_by == loggeduser:
                return super(PlaceEditAuthMixin, self).dispatch(request, *args, **kwargs)
            else:
                return redirect('core:update_unauth')


class PlaceDetailViewAuthMixin:
    """
    Permission d'éditer le détail de la localité dans les conditions suivantes:
        L'utilisateur est superutilisateur,
        L'utilisateur peut modifier Toutes les données,
        L'utilisateur est responsable du territoire de la localité,
        L'utilisateur est créateur de la localité.
    """

    def dispatch(self, request, *args, **kwargs):
        self.obj = self.get_object()
        if not request.user.is_authenticated:
            return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
        else:
            loggeduser = get_user_model().objects.get(id=request.user.id)
            if loggeduser.is_superuser or loggeduser.edit_all_data or loggeduser.access_all_data or (
                    loggeduser.is_resp and self.obj.place.territory in loggeduser.resp_territory.all()) \
                    or self.obj.created_by == loggeduser:
                return super(PlaceDetailViewAuthMixin, self).dispatch(request, *args, **kwargs)
            else:
                return redirect('core:update_unauth')


class PlaceDetailEditAuthMixin:
    """
    Permission d'éditer le détail de la localité dans les conditions suivantes:
        L'utilisateur est superutilisateur,
        L'utilisateur peut modifier Toutes les données,
        L'utilisateur est responsable du territoire de la localité,
        L'utilisateur est créateur de la localité.
    """

    def dispatch(self, request, *args, **kwargs):
        self.obj = self.get_object()
        if not request.user.is_authenticated:
            return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
        else:
            loggeduser = get_user_model().objects.get(id=request.user.id)
            if loggeduser.is_superuser or loggeduser.edit_all_data or (
                    loggeduser.is_resp and self.obj.place.territory in loggeduser.resp_territory.all()) \
                    or self.obj.created_by == loggeduser:
                return super(PlaceDetailEditAuthMixin, self).dispatch(request, *args, **kwargs)
            else:
                return redirect('core:update_unauth')


class TreeGiteViewAuthMixin:
    """
    Permission d'éditer le détail de la localité dans les conditions suivantes:
        L'utilisateur est superutilisateur,
        L'utilisateur peut modifier Toutes les données,
        L'utilisateur est responsable du territoire de la localité,
        L'utilisateur est créateur de la donnée.
    """

    def dispatch(self, request, *args, **kwargs):
        self.obj = self.get_object()
        if not request.user.is_authenticated:
            return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
        else:
            loggeduser = get_user_model().objects.get(id=request.user.id)
            if loggeduser.is_superuser or loggeduser.edit_all_data or loggeduser.access_all_data or (
                    loggeduser.is_resp and self.obj.tree.place.territory in loggeduser.resp_territory.all()) or self.obj.created_by == loggeduser:
                return super(TreeGiteViewAuthMixin, self).dispatch(request, *args, **kwargs)
            else:
                return redirect('core:update_unauth')


class TreeGiteEditAuthMixin:
    """
    Permission d'éditer le détail de la localité dans les conditions suivantes:
        L'utilisateur est superutilisateur,
        L'utilisateur peut modifier Toutes les données,
        L'utilisateur est responsable du territoire de la localité,
        L'utilisateur est créateur de la donnée.
    """

    def dispatch(self, request, *args, **kwargs):
        self.obj = self.get_object()
        if not request.user.is_authenticated:
            return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
        else:
            loggeduser = get_user_model().objects.get(id=request.user.id)
            if loggeduser.is_superuser or loggeduser.edit_all_data or (
                    loggeduser.is_resp and self.obj.tree.place.territory in loggeduser.resp_territory.all()) \
                    or self.obj.created_by == loggeduser:
                return super(TreeGiteEditAuthMixin, self).dispatch(request, *args, **kwargs)
            else:
                return redirect('core:update_unauth')


class SessionViewAuthMixin:
    '''
    Permet de voir la session dans les conditions suivantes:
        L'utilisateur est superutilisateur,
        L'utilisateur peut voir toutes les données,
        L'utilisateur est responsable du territoire de la localité de cette session,
        L'utilisateur est créateur de la session.
    '''

    def dispatch(self, request, *args, **kwargs):
        self.obj = self.get_object()
        if not request.user.is_authenticated:
            return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
        else:
            loggeduser = get_user_model().objects.get(id=request.user.id)
            if loggeduser.is_superuser or loggeduser.edit_all_data or loggeduser.access_all_data or (
                    loggeduser.is_resp and self.obj.place.territory in loggeduser.resp_territory.all()) \
                    or self.obj.created_by == loggeduser or self.obj.main_observer == loggeduser \
                    or loggeduser in self.obj.other_observer.all():
                return super(SessionViewAuthMixin, self).dispatch(request, *args, **kwargs)
            else:
                return redirect('core:update_unauth')


class SessionEditAuthMixin:
    '''
    Permet de voir la session dans les conditions suivantes:
        L'utilisateur est superutilisateur,
        ou L'utilisateur peut voir toutes les données,
        ou L'utilisateur est responsable du territoire de la localité de cette session,
        ou L'utilisateur est créateur de la session
        ou L'utilisateur est l'observateur principal de la session
        ou L'utilisateur est l'un des autres observateurs de la session
    '''

    def dispatch(self, request, *args, **kwargs):
        self.obj = self.get_object()
        if not request.user.is_authenticated:
            return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
        else:
            loggeduser = get_user_model().objects.get(id=request.user.id)
            if loggeduser.is_superuser or loggeduser.edit_all_data or (
                    loggeduser.is_resp and self.obj.place.territory in loggeduser.resp_territory.all()) \
                    or self.obj.created_by == loggeduser or self.obj.main_observer == loggeduser \
                    or loggeduser in self.obj.other_observer.all():
                return super(SessionEditAuthMixin, self).dispatch(request, *args, **kwargs)
            else:
                return redirect('core:update_unauth')


class SightingViewAuthMixin:
    '''
    Permet de voir l'observation dans les conditions suivantes:
        L'utilisateur est superutilisateur,
        L'utilisateur peut voir toutes les données,
        L'utilisateur est responsable du territoire de la localité de cette session,
        L'utilisateur est créateur de la session de la donnée.
    '''

    def dispatch(self, request, *args, **kwargs):
        self.obj = self.get_object()
        if not request.user.is_authenticated:
            return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
        else:
            loggeduser = get_user_model().objects.get(id=request.user.id)
            if loggeduser.is_superuser or loggeduser.edit_all_data or loggeduser.access_all_data or (
                    loggeduser.is_resp and self.obj.session.place.territory in loggeduser.resp_territory.all()) \
                    or self.obj.created_by == loggeduser or self.obj.session.main_observer == loggeduser \
                    or loggeduser in self.obj.session.other_observer.all():
                return super(SightingViewAuthMixin, self).dispatch(request, *args, **kwargs)
            else:
                return redirect('core:update_unauth')


class SightingEditAuthMixin:
    '''
    Permet de voir l'observation dans les conditions suivantes:
        L'utilisateur est superutilisateur  :get_user_model():is_superuser is True,
        L'utilisateur peut voir toutes les données :get_user_model():edit_all_data is True,
        L'utilisateur est responsable du territoire de la localité de cette session :get_user_model():is_resp is True & territory in :get_user_model():resp_territory,
        L'utilisateur est créateur de la session de la donnée
        L'utilisateur est l'observateur principal de la session
    '''

    def dispatch(self, request, *args, **kwargs):
        self.obj = self.get_object()
        if not request.user.is_authenticated:
            return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
        else:
            loggeduser = get_user_model().objects.get(id=request.user.id)
            if loggeduser.is_superuser or loggeduser.edit_all_data or (
                    loggeduser.is_resp and self.obj.session.place.territory in loggeduser.resp_territory.all()) \
                    or self.obj.created_by == loggeduser or self.obj.session.main_observer == loggeduser:
                return super(SightingEditAuthMixin, self).dispatch(request, *args, **kwargs)
            else:
                return redirect('core:update_unauth')


class CountDetailViewAuthMixin:
    '''
    Permet de voir l'observation dans les conditions suivantes:
        L'utilisateur est superutilisateur,
        L'utilisateur peut voir toutes les données,
        L'utilisateur est responsable du territoire de la localité de cette session,
        L'utilisateur est créateur de la session de la donnée
        L'utilisateur est l'un des observateurs de la session
    '''

    def dispatch(self, request, *args, **kwargs):
        self.obj = self.get_object()
        if not request.user.is_authenticated:
            return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
        else:
            loggeduser = get_user_model().objects.get(id=request.user.id)
            if loggeduser.is_superuser or loggeduser.edit_all_data or loggeduser.access_all_data or (
                    loggeduser.is_resp and self.obj.sighting.session.place.territory in loggeduser.resp_territory.all()) \
                    or self.obj.created_by == loggeduser or self.obj.sighting.session.main_observer == loggeduser \
                    or loggeduser in self.obj.sighting.session.other_observer.all():
                return super(CountDetailViewAuthMixin, self).dispatch(request, *args, **kwargs)
            else:
                return redirect('core:update_unauth')


class CountDetailEditAuthMixin:
    '''
    Permet de voir l'observation dans les conditions suivantes:
        L'utilisateur est superutilisateur  :get_user_model():is_superuser is True,
        L'utilisateur peut voir toutes les données :get_user_model():edit_all_data is True,
        L'utilisateur est responsable du territoire de la localité de cette session :get_user_model():is_resp is True & territory in :get_user_model():resp_territory,
        L'utilisateur est créateur de la session de la donnée
        L'utilisateur est l'observateur principal de la session
    '''

    def dispatch(self, request, *args, **kwargs):
        self.obj = self.get_object()
        if not request.user.is_authenticated:
            return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
        else:
            loggeduser = get_user_model().objects.get(id=request.user.id)
            if loggeduser.is_superuser or loggeduser.edit_all_data or (
                    loggeduser.is_resp and self.obj.sighting.session.place.territory in loggeduser.resp_territory.all()) \
                    or self.obj.created_by == loggeduser or self.obj.sighting.session.main_observer == loggeduser:
                return super(CountDetailEditAuthMixin, self).dispatch(request, *args, **kwargs)
            else:
                return redirect('core:update_unauth')


class SightingListAuthMixin(LoginRequiredMixin):
    '''
    Classe mixin de vérification que l'utilisateur possède les droits de voir les données (view_all_data, edit_all_data ou is_resp):
    Soit il est créateur de la donnée,
    Soit il dispose de l'authorisation de modifier toutes les données ( :get_user_model():access_all_data is True)
    '''

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
        else:
            loggeduser = get_user_model().objects.get(id=request.user.id)
            if loggeduser.edit_all_data or loggeduser.access_all_data or loggeduser.is_resp:
                print('SightingListAuthMixin : access granted')
                return super(SightingListAuthMixin, self).dispatch(request, *args, **kwargs)
            else:
                return redirect('core:view_unauth')
