import nested_admin
from django.contrib import admin
from leaflet.admin import LeafletGeoAdminMixin

from .models import (CountDetail, Place,
                     Bridge, Build, Cave,
                     Tree, Session, Sighting, Transmitter,
                     Study)


# # =========================================================================
# # Set nested inline formsfor DictSpecie submodels
# # =========================================================================
#
#
# class DictSpecieDetailInline(nested_admin.NestedTabularInline):
#     model = DictSpecieDetail
#     can_delete = False
#     verbose_name_plural = "Détails de l'espèce"
#
#
# class DictSpecieStatusInline(nested_admin.NestedTabularInline):
#     model = DictSpecieStatus
#     can_delete = False
#     verbose_name_plural = "Détails des statuts de l'espèce"
#
#
# class DictSpecieAdmin(admin.ModelAdmin):
#     inlines = (DictSpecieDetailInline, DictSpecieStatusInline,)


# # =========================================================================
# # Set nested inline formsfor User submodels
# # =========================================================================
#
#
# class UserDetailInline(LeafletGeoAdminMixin, nested_admin.NestedStackedInline):
#     model = UserDetail
#     can_delete = False
#     verbose_name_plural = 'Details de utilisateur'
#
#
# class UserAdmin(nested_admin.NestedModelAdmin):
#     inlines = (UserDetailInline,)
#     list_display = ['id','username','email','first_name','last_name','is_superuser','is_staff','is_active','last_login']
#     search_fields = ['username','email','first_name','last_name']
#     list_filter = ('is_active','is_staff','is_superuser')

# =========================================================================
# Set nested inline forms for Place & Sight submodels
# =========================================================================


class CountDetailInLine(nested_admin.NestedStackedInline):
    model = CountDetail
    extra = 0
    fk_name = 'sighting'
    readonly_fields = ('etat_sexuel',)
    fieldsets = (
        (None, {'fields': (('sex', 'age'), ('precision', 'count', 'unit'))}),
        ('Informations complémentaires', {'classes': ('grp-collapse grp-open',),
                                          'fields': (('time', 'manipulator', 'validator', 'transmitter'),
                                                     ('comment', 'etat_sexuel'))}),
        ('Données biométriques', {'classes': ('collapse', 'grp-collapse grp-open',),
                                  'fields': (('ab', 'd5', 'd3', 'pouce'), ('queue', 'tibia', 'cm3',
                                                                           'tragus'),
                                             ('poids', 'testicule', 'epididyme', 'tuniq_vag'),
                                             ('gland_taille', 'gland_coul', 'mamelle', 'gestation'),
                                             ('epiphyse', 'chinspot', 'usure_dent'))}),
    )

    def save_model(self, request, obj, form, change):
        if getattr(obj, 'created_by', None) is None:
            obj.created_by = request.user
        obj.save()

    def get_queryset(self, request):
        qs = super(CountDetailInLine, self).get_queryset(request)
        if request.user.is_staff and request.user.is_superuser:
            return qs
        return qs.filter(created_by=request.user)


class SightingInLine(nested_admin.NestedTabularInline):
    model = Sighting
    extra = 0
    inlines = [CountDetailInLine, ]

    def save_model(self, request, obj, form, change):
        if getattr(obj, 'created_by', None) is None:
            obj.created_by = request.user
        obj.save()

    def get_queryset(self, request):
        qs = super(SightingInLine, self).get_queryset(request)
        if request.user.is_staff and request.user.is_superuser:
            return qs
        return qs.filter(created_by=request.user)


class SightingAdmin(nested_admin.NestedModelAdmin):
    inlines = [
        CountDetailInLine,
    ]

    def save_model(self, request, obj, form, change):
        if getattr(obj, 'created_by', None) is None:
            obj.created_by = request.user
        obj.save()

    def get_queryset(self, request):
        qs = super(SightingAdmin, self).get_queryset(request)
        if request.user.is_staff and request.user.is_superuser:
            return qs
        return qs.filter(created_by=request.user)


class SessionInLine(nested_admin.NestedStackedInline):
    model = Session
    extra = 0
    inlines = [SightingInLine, ]
    fieldsets = (
        (None, {'fields': ('name', 'place', 'method', 'date_start',
                           'date_end', 'main_observer')}),
        ('Commentaire', {'classes': ('collapse',
                                     'grp-collapse grp-open',), 'fields': ('comment',)}),
    )

    def save_model(self, request, obj, form, change):
        if getattr(obj, 'created_by', None) is None:
            obj.created_by = request.user
        obj.save()


class SessionAdmin(nested_admin.NestedModelAdmin):
    inlines = [SightingInLine, ]
    fieldsets = (
        (None, {'fields': (('name', 'place'), ('method', 'main_observer'), ('date_start',
                                                                            'date_end'),)}),
        ('Commentaire', {'classes': ('collapse',
                                     'grp-collapse grp-open',), 'fields': ('comment',)}),
    )

    def save_model(self, request, obj, form, change):
        if getattr(obj, 'created_by', None) is None:
            obj.created_by = request.user
        obj.save()


class PlaceBridgeDetailInline(nested_admin.NestedStackedInline):
    model = Bridge
    extra = 0
    can_delete = False
    verbose_name_plural = "Détails du pont"

    def save_model(self, request, obj, form, change):
        if getattr(obj, 'created_by', None) is None:
            obj.created_by = request.user
        obj.save()


class PlaceBuildDetailInline(nested_admin.NestedStackedInline):
    model = Build
    extra = 0
    can_delete = False
    verbose_name_plural = "Détails du bâtiment"

    def save_model(self, request, obj, form, change):
        if getattr(obj, 'created_by', None) is None:
            obj.created_by = request.user
        obj.save()


class PlaceTreeDetailInline(nested_admin.NestedStackedInline):
    model = Tree
    extra = 0
    can_delete = False
    verbose_name_plural = "Détails d'un arbre"

    def save_model(self, request, obj, form, change):
        if getattr(obj, 'created_by', None) is None:
            obj.created_by = request.user
        obj.save()


class PlaceCaveDetailInline(LeafletGeoAdminMixin, nested_admin.NestedStackedInline):
    model = Cave
    extra = 0
    can_delete = False
    verbose_name_plural = "Détails d'une cavité"

    def save_model(self, request, obj, form, change):
        if getattr(obj, 'created_by', None) is None:
            obj.created_by = request.user
        obj.save()


class PlaceAdmin(LeafletGeoAdminMixin, nested_admin.NestedModelAdmin):
    model = Place
    inlines = [PlaceBridgeDetailInline,
               PlaceBuildDetailInline, PlaceTreeDetailInline, PlaceCaveDetailInline, SessionInLine, ]
    list_filter = ('territory',
                   ('created_by', admin.RelatedOnlyFieldListFilter)
                   )
    list_display = ['name', 'territory',
                    'timestamp_create', 'timestamp_update', 'is_hidden', 'type', 'is_gite', 'convention',
                    'convention_file', 'photo_file', 'plan_localite']
    search_fields = ['name', 'created_by']
    sortable_field_name = ['name', 'municipality', 'territory'
                                                   'timestamp_create', 'timestamp_update']
    autocomplete_lookup_fields = {
        'name': ['name'],
    }
    readonly_fields = ('x', 'y',)
    fieldsets = (
        (None, {'fields': (('name', 'precision'),
                           ('is_hidden', 'type', 'domain'), 'geom')}),
        ('Gites et refuges', {'classes': ('collapse', 'grp-collapse grp-open',),
                              'fields': (('proprietary', 'is_gite'), ('convention', 'convention_file'))}),
        ('Autres informations', {'classes': ('collapse', 'grp-collapse grp-open',),
                                 'fields': (('bdsource', 'id_bdsource', 'territory'),
                                            ('photo_file', 'id_bdcavite', 'plan_localite'))}),
        ('Commentaire', {'classes': ('collapse', 'grp-collapse grp-open',),
                         'fields': ('comment',)}),
    )

    # =========================================================================
    # fieldsets = (
    #     (None, {'fields': (('name', 'precision', 'territory', 'domain', 'plan_localite'), ('type', 'is_hidden', 'habitat', 'altitude', 'photo_file'), ('is_gite',
    #                                                                                                                                                 'convention', 'convention_file'),  'geom')}),
    #     ('Autres infos', {'classes': ('collapse', 'grp-collapse grp-open',),
    #                       'fields': (('x_l93', 'y_l93'), ('municipality', 'department', 'region'))}),
    # )
    # =========================================================================

    def save_model(self, request, obj, form, change):
        if getattr(obj, 'created_by', None) is None:
            obj.created_by = request.user
        obj.save()

    def get_formsets(self, request, obj=type):
        for inline in self.get_inline_instances(request, obj):
            if isinstance(inline, PlaceBridgeDetailInline) and obj is 'pont':
                continue
            yield inline.get_formset(request, obj)

            # =========================================================================
            # def save_model(self, request, obj, form, change):
            #     if getattr(obj, 'author', None) is None:
            #         obj.author = Territory.object.filter(geom__contains=geom)
            #     obj.save()
            # =========================================================================


# class CommentAdmin(admin.ModelAdmin):
#     pass
#
#     def save_model(self, request, obj, form, change):
#         if getattr(obj, 'created_by', None) is None:
#             obj.created_by = request.user
#         obj.save()


# class CatchAuthAdmin(admin.ModelAdmin):
#     model = CatchAuth
#     list_display = ('id_catchauth', 'date_start', 'date_end',
#                     'territory', 'official_reference', 'file')
#     search_fields = ('territory',)
#     sortable_field_name = ['__all__']


class ActuAdmin(admin.ModelAdmin):
    list_display = ('title', 'author', 'timestamp_create', 'timestamp_update')
    fieldsets = [
        (None, {'fields': ('title', 'briefdescr', 'text')}),
    ]

    def save_model(self, request, obj, form, change):
        if getattr(obj, 'author', None) is None:
            obj.author = request.user
        obj.save()


# admin.site.unregister(User)
# admin.site.register(User, UserAdmin)
# admin.site.register(Territory,LeafletGeoAdmin)
admin.site.register(Place, PlaceAdmin)
admin.site.register(Sighting, SightingAdmin)
# admin.site.register(DictSpecie, DictSpecieAdmin)
# admin.site.register(CatchAuth, CatchAuthAdmin)

admin.site.register(Session, SessionAdmin)
admin.site.register(Transmitter)
# admin.site.register(Comment, CommentAdmin)
admin.site.register(Study)
